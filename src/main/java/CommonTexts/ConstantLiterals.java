package CommonTexts;

public final class ConstantLiterals {

	private ConstantLiterals(){};
	public static final String MajorDimension_Column 						= "COLUMNS";
	public static final String MajorDimension_Row 							= "ROWS";
	public static final String ResourcesPath 								= "src/main/resources/";
	public static final String APPLICATION_NAME 							= "Google_Sheet";
	public static final String GSHEETCREDENTIALSPATH  						= ".credentials/sheets.googleapis.com-orderState";
	public static final String CalculationSheetRange 						= "Calculation!A2:E";
	public static final String GSheetTest 									= "1izSMMj5KUbi99gXgc5i4OMPz5Xl0qU09GBAlVP6RIBs";
}
